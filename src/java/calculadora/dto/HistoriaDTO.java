/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculadora;

/**
 *
 * @author amuract
 */
public class HistoriaDTO {
    private String sentencia; 
    private String resultado; 
    private Integer session; 

    public HistoriaDTO(){
    }
    
    public HistoriaDTO(String sentencia, String resultado, Integer session) {
        this.sentencia = sentencia;
        this.resultado = resultado;
        this.session = session;
    }
    
    public String getSentencia() {
        return sentencia;
    }

    public void setSentencia(String sentencia) {
        this.sentencia = sentencia;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public Integer getSession() {
        return session;
    }

    public void setSession(Integer session) {
        this.session = session;
    }
    
    public String toString(){
        return this.sentencia +" = "+this.resultado+"  -> Sesion: "+this.session; 
    }
}
