package calculadora;

import calculadora.dao.HistoriaDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

/**
 * @author amuract
 */
@Stateless
@Path("/")
public class Calculadora {

    @EJB
    private CalculadoraBean calc;

    /**
     * Retrieves representation of an instance of helloworld.HelloWorldResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getPrincipal() {
        String html = getHtml();
        String ejemplo = ""
                + "		<p>\n"
                + "			Estilo de ecuaciones que el motor reconoce. Tener en cuenta que el log toma dos parametros el primero la base y va pegada a la palabra log el segundo es el numero y va entre parentesis sin espacio, es decir log2(32) es el logaritmo de 32 en base dos\n"
                + "		</p>\n"
                + "		<p>2 * (1 + 3) = 8</p>\n"
                + "		<p>2 * 1 + 3 = 5</p>\n"
                + "		<p>(2 * 2) + 3 = 7</p>\n"
                + "		<p>(2.7 * 2.1) + 3.3 = 8.97</p>\n"
                + "		<p>(2.73215546 * 2.17986565) + 3.34213546 = 9.29786729771395</p>\n"
                + "		<p>((8-2)+((2+2)/(4-2))+log2(32)) = 6 + 2 + 5 = 13</p>\n"
                + "		<p>((8-2)*log2(32)+((2+2)/(4-2))+log2(32)) = 6 * 5 + 2 + 5 = 37</p>\n"
                + "		<p>((8-2)*log2(8)+((2+2)/(4-2))+log2(32)) = 6 * 3 + 2 + 5 = 25</p>\n"
                + "		<p>etc</p>\n";

        html = html.replaceAll("##EJEMPLO##", ejemplo);
        html = html.replaceAll("##ECUACION_ACTUAL##", "");
        html = html.replaceAll("##RESULTADO##", "");
        html = html.replaceAll("##HISTORIAL##", "");
        html = html.replaceAll("##HISTORIAL_COMPLETO##", "");

        return html;
    }

    /**
     * Respuesta JSON
     */
    @GET
    @Path("json/session/")
    @Produces("application/json")
    public List<HistoriaDTO> getSessionJSON() {
        return new ArrayList<HistoriaDTO>(calc.getSessionJSON());
    }

    @GET
    @Path("json/session/{session}")
    @Produces("application/json")
    public List<HistoriaDTO> getSessionJSON(@PathParam("session") Integer session) {
        return new ArrayList<HistoriaDTO>(calc.getSessionJSON(session));
    }

    @GET
    @Path("json/calcular/{session}/{sentencia}")
    @Produces("application/json")
    public HistoriaDTO calcularJSON(@PathParam("sentencia") String sentencia, @PathParam("session") Integer session) {
        return calc.calcularJSON(sentencia, session);
    }

    @POST
    @Path("json/calcular/post")
    @Consumes("application/x-www-form-urlencoded")
    public HistoriaDTO calcularPostJSON(@FormParam("session") Integer session, @FormParam("sentencia") String sentencia) {
        return calc.calcularJSON(sentencia, session);
    }

    /**
     * Respuesta HTML *
     */
    @GET
    @Path("session/{session}")
    @Produces("text/html")
    public String getSession(@PathParam("session") Integer session) {
        String html = getHtml();
        html = html.replaceAll("##EJEMPLO##", "");
        html = html.replaceAll("##ECUACION_ACTUAL##", "");
        html = html.replaceAll("##RESULTADO##", "");
        html = html.replaceAll("##HISTORIAL##", "Historial de la session: " + calc.getSession(session));
        html = html.replaceAll("##HISTORIAL_COMPLETO##", "");

        return html;
    }

    @GET
    @Path("session/")
    @Produces("text/html")
    public String getSession() {
        String html = getHtml();
        html = html.replaceAll("##EJEMPLO##", "");
        html = html.replaceAll("##ECUACION_ACTUAL##", "");
        html = html.replaceAll("##RESULTADO##", "");
        html = html.replaceAll("##HISTORIAL##", "");
        html = html.replaceAll("##HISTORIAL_COMPLETO##", "Historial completo: " + calc.getSession());

        return html;
    }

    @GET
    @Path("calcular/{session}/{sentencia}")
    @Produces("text/html")
    public String calcular(@PathParam("sentencia") String sentencia, @PathParam("session") Integer session) {
        String html = getHtml();
        html = html.replaceAll("##EJEMPLO##", "");
        html = html.replaceAll("##ECUACION_ACTUAL##", "Ecuación calculada: " + sentencia);
        html = html.replaceAll("##RESULTADO##", "Resultado de la Ecuación: " + calc.calcular(sentencia, session));
        html = html.replaceAll("##HISTORIAL##", "Historial de la session: " + calc.getSession(session));
        html = html.replaceAll("##HISTORIAL_COMPLETO##", "Historial completo: " + calc.getSession());

        return html;
    }

    @POST
    @Path("calcular/post")
    @Consumes("application/x-www-form-urlencoded")
    public String calcularPost(@FormParam("session") Integer session, @FormParam("sentencia") String sentencia) {
        String html = getHtml();
        html = html.replaceAll("##EJEMPLO##", "");
        html = html.replaceAll("##ECUACION_ACTUAL##", "Ecuación calculada: " + sentencia);
        html = html.replaceAll("##RESULTADO##", "Resultado de la Ecuación: " + calc.calcular(sentencia, session));
        html = html.replaceAll("##HISTORIAL##", "Historial de la session: " + calc.getSession(session));
        html = html.replaceAll("##HISTORIAL_COMPLETO##", "Historial completo: " + calc.getSession());
        return html;
    }

    
    @GET
    @Path("clearDB/")
    @Produces("text/html")
    public String clearDB() {
        HistoriaDAO dao = new HistoriaDAO(); 
        try {
            dao.clearDB();
        } catch (SQLException ex) {
            Logger.getLogger(Calculadora.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "DB limpia";
    }
    
    private String getHtml() {
        String html
                = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "    <head>\n"
                + "        <title>Calculadora simple</title>\n"
                + "        <meta charset=\"UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width\">\n"
                + "    </head>\n"
                + "    <body>\n"
                + "	<div id=\"ejemplo\">\n"
                + "	      ##EJEMPLO##"
                + "	</div>"
                + "        <div id=\"ecuacion_actual\">\n"
                + "            ##ECUACION_ACTUAL##\n"
                + "        </div>\n"
                + "        <div id=\"resultado\">\n"
                + "            ##RESULTADO##\n"
                + "        </div>\n"
                + "        <div id=\"historial\">\n"
                + "            ##HISTORIAL##\n"
                + "        </div>\n"
                + "        <div id=\"historial_completo\">\n"
                + "            ##HISTORIAL_COMPLETO##\n"
                + "        </div>\n"
                + "\n"
                + "    </body>\n"
                + "</html>\n"
                + "";
        return html;
    }

}
