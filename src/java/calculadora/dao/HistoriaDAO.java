/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora.dao;

import calculadora.HistoriaDTO;
import java.sql.Array;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author amuract
 */
public class HistoriaDAO {

    public HistoriaDAO() {
        try {
            createDB();
        } catch (SQLException ex) {
            Logger.getLogger(HistoriaDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error al crear la base de datos");
        }
    }

    public void createDB() throws SQLException {
        String nombreDB = "historia.db";
        try {
            Class.forName("org.sqlite.JDBC");//especificamos el driver
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + nombreDB);//creamos la conexión, si la BD no existe la crea
            Statement stat = conn.createStatement();
            stat.executeUpdate("CREATE TABLE IF NOT EXISTS historia(id INTEGER,expresion_algebraica VARCHAR(200),resultado VARCHAR(50),session INTEGER);");
            stat.close();
            conn.close();//Finalmente cerramos la conexion
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void clearDB() throws SQLException {
        String nombreDB = "historia.db";
        try {
            Class.forName("org.sqlite.JDBC");//especificamos el driver
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + nombreDB);//creamos la conexión, si la BD no existe la crea
            Statement stat = conn.createStatement();
            stat.executeUpdate("delete from historia;");
            stat.close();
            conn.close();//Finalmente cerramos la conexion
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @param sent
     * @param resultado (se define como String para evitar problemas de
     * compatibilidad en sql lite, dado que no contiene el float)
     * @throws SQLException
     */
    public void insertDB(HistoriaDTO dto) throws SQLException {
        String nombreDB = "historia.db";
        try {
            Class.forName("org.sqlite.JDBC");//especificamos el driver
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + nombreDB);//creamos la conexión, si la BD no existe la crea
            Statement stat = conn.createStatement();
            String query = "INSERT INTO historia ('expresion_algebraica', 'resultado', 'session') VALUES ('##EXPRESION##', '##RESULT##', ##SESSION##);";
            query = query.replace("##EXPRESION##", dto.getSentencia());
            query = query.replace("##RESULT##", dto.getResultado());
            query = query.replace("##SESSION##", dto.getSession().toString());
            stat.executeUpdate(query);
            stat.close();
            conn.close();//Finalmente cerramos la conexion
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList selectSession(Integer session) {
        String nombreDB = "historia.db";
        try {
            Class.forName("org.sqlite.JDBC");//especificamos el driver
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + nombreDB);//creamos la conexión, si la BD no existe la crea
            Statement stat = conn.createStatement();
            String query = "SELECT * FROM historia WHERE  session = ##SESSION##;"; // no hace falta ordenar dado que el resultado vuelve ordenado por ID; 
            query = query.replace("##SESSION##", session.toString());
            ResultSet rs = stat.executeQuery(query);

            ArrayList dtos = new ArrayList<HistoriaDTO>();
            while (rs.next()) {//recorremos todas las filas                

                dtos.add(new HistoriaDTO(rs.getString("expresion_algebraica"), rs.getString("resultado"), new Integer(rs.getString("session"))));
            }

            stat.close();
            conn.close();//Finalmente cerramos la conexion

            return dtos;

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public ArrayList<HistoriaDTO> selectAllSession() {
        String nombreDB = "historia.db";
        try {
            Class.forName("org.sqlite.JDBC");//especificamos el driver
            Connection conn = DriverManager.getConnection("jdbc:sqlite:" + nombreDB);//creamos la conexión, si la BD no existe la crea
            Statement stat = conn.createStatement();
            String query = "SELECT * FROM historia;"; // no hace falta ordenar dado que el resultado vuelve ordenado por ID; 
            ResultSet rs = stat.executeQuery(query);

            ArrayList dtos = new ArrayList<HistoriaDTO>();
            while (rs.next()) {
                dtos.add(new HistoriaDTO(rs.getString("expresion_algebraica"), rs.getString("resultado"), new Integer(rs.getString("session"))));
            }
            stat.close();
            conn.close();//Finalmente cerramos la conexion

            return dtos;

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int getAllSession() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
