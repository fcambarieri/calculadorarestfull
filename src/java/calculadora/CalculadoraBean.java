/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 * 
 * Contributor(s):
 * 
 * Portions Copyrighted 2008 Sun Microsystems, Inc.
 */
package calculadora;

import calculadora.dao.HistoriaDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author amuract
 */
@Singleton
public class CalculadoraBean {

    private HistoriaDTO dto;
    private HistoriaDAO dao;

    public CalculadoraBean() {
        dto = new HistoriaDTO();
        dao = new HistoriaDAO();
    }

//    public String calcular(String str, Integer session) {
//        CalculadoraBO cbo = new CalculadoraBO();
//        return cbo.calculate(str, session).getResultado();
//    }
//    public String getHistorial(Integer session) {
//        CalculadoraBO cbo = new CalculadoraBO();
//        return cbo.getHistoria(session);
//    }
//    
//    public String getHistorial() {
//        CalculadoraBO cbo = new CalculadoraBO();
//        return cbo.getHistorial();
//    }
//    public List<HistoriaDTO> getSessionJSON(Integer session) {
//        
//    }
    /**
     * Logaritmo del num en base definida
     *
     * @param num
     * @param base
     * @return
     */
    private Float log(Float num, Float base) {
        return new Float(Math.log(num) / Math.log(base));
    }

    /**
     * JSON
     */
    /**
     * @return el historial de sentencias calculadas en el orden en que se
     * fueron ejecutando
     */
    public List<HistoriaDTO> getSessionJSON() {
        return dao.selectAllSession();
    }

    public List<HistoriaDTO> getSessionJSON(Integer session) {
        return dao.selectSession(session);
    }

    /**
     * Dada una expresion algebraica, la calcula, retorna el resultado y
     * almacena la misma y el resultado en una DB para su posterior recuperación
     *
     * @param sent
     * @param session
     * @return
     */
    public HistoriaDTO calcularJSON(String sent, Integer session) {
        String str = sent;
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");

        while (str.contains("log")) {
            String base = str.substring(
                    str.indexOf("log") + 3,
                    str.indexOf("(", str.indexOf("log"))
            );
            String number = str.substring(
                    str.indexOf("(", str.indexOf("log")) + 1,
                    str.indexOf(")", str.indexOf("log"))
            );

            Float logResult = log(new Float(number), new Float(base));
            str = str.replace(str.substring(str.indexOf("log"), str.indexOf(")", str.indexOf("log")) + 1), logResult.toString());
        }
        try {
            String resultado = engine.eval(str).toString();

            dto.setSentencia(sent);
            dto.setResultado(resultado);
            dto.setSession(session);
            dao.insertDB(dto);

            return dto;

        } catch (ScriptException ex) {
            Logger.getLogger(CalculadoraBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CalculadoraBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * @param session
     * @return el historial de sentencias calculadas en la session
     */
    public String getSession(Integer session) {
        List<HistoriaDTO> dtos = new ArrayList<HistoriaDTO>();
        String result = "";
        dtos = getSessionJSON(session);
        for (int i = 0; i < dtos.size(); i++) {
            result = result + dtos.get(i).toString();
        }
        return result;
    }

    /**
     * @param session
     * @return el historial de sentencias calculadas en la session
     */
    public String getSession() {
        List<HistoriaDTO> dtos = new ArrayList<HistoriaDTO>();
        String result = "";
        dtos = getSessionJSON();
        for (int i = 0; i < dtos.size(); i++) {
            result = result + dtos.get(i).toString();
        }
        return result;
    }

    public String calcular(String sent, Integer session) {
        return calcularJSON(sent, session).getResultado();
    }

}
